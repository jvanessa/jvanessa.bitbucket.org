
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";




/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Kreiraj nov EHR zapis za pacienta in dodaj osnovne demografske podatke.
 * V primeru uspešne akcije izpiši sporočilo s pridobljenim EHR ID, sicer
 * izpiši napako.
 */
function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();
    var datumRojstva = $("#kreirajDatumRojstva").val() + "T00:00:00.000Z";
    var visina = $("#kreirajVisino").val();
    var teza = $("#kreirajTezo").val();
    var Stlak = $("#kreirajStlak").val();
    var Dtlak = $("#kreirajDtlak").val();
    
    var umes = datumRojstva.split("-");
    var starost = 2018 -umes[0];

	if (!ime || !priimek || !datumRojstva || !visina || !teza || !Stlak|| !Dtlak ||ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0 || 
      Stlak.trim().length == 0 || Dtlak.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'><font color='white'>Prosim vnesite zahtevane podatke!</font></span>");
	} else {
		$.ajaxSetup({
		    headers: {"Ehr-Session": sessionId}
		});
		$.ajax({
		    url: baseUrl + "/ehr",
		    type: 'POST',
		    success: function (data) {
		        var ehrId = data.ehrId;
		        var partyData = {
		            firstNames: ime,
		            lastNames: priimek,
		            dateOfBirth: datumRojstva,
		            partyAdditionalInfo: [{key: "height", value: visina} ,{key: "weight", value: teza}, {key: "systolic", value: Stlak}, {key: "diastolic", value: Dtlak}, {key: "ehrId", value: ehrId}]
		        };
		        $.ajax({
		            url: baseUrl + "/demographics/party",
		            type: 'POST',
		            contentType: 'application/json',
		            data: JSON.stringify(partyData),
		            success: function (party) {
		                if (party.action == 'CREATE') {
		                    $("#kreirajSporocilo").html("<span class='obvestilo " +
                          "label label-success fade-in'><font color='white'>Uspešno kreiran EHR '" +
                          ehrId + "'.</font></span>");
		                    $("#preberiEHRid").val(ehrId);
		                    
		                    var datumInUra = "2018-05-27T11:40Z";
							var telesnaVisina = visina;
							var telesnaTeza = teza;
							var telesnaTemperatura = "37.0";
							var sistolicniKrvniTlak = Stlak;
							var diastolicniKrvniTlak = Dtlak;
							var nasicenostKrviSKisikom = "94";
							var merilec = "Vanessa Jacmenjak";
						
							if (!ehrId || ehrId.trim().length == 0) {
								$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
							} else {
								$.ajaxSetup({
								    headers: {"Ehr-Session": sessionId}
								});
								var podatki = { // source predloge za vital signs
									// Preview Structure: https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
								    "ctx/language": "en",
								    "ctx/territory": "SI",
								    "ctx/time": datumInUra,
								    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
								    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
								   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
								    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
								    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
								    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
								    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
								};
								var parametriZahteve = { // klic template-a
								    "ehrId": ehrId,
								    templateId: 'Vital Signs', // template id
								    format: 'FLAT',
								    committer: merilec
								};
								$.ajax({
								    url: baseUrl + "/composition?" + $.param(parametriZahteve),
								    type: 'POST',
								    contentType: 'application/json',
								    data: JSON.stringify(podatki),
								    success: function (res) { // pošiljanje podatkov na server
								    	console.log(res.meta.href);
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-success fade-in'><font color='white'>Uspešno kreiran EHR '" + ehrId + "'.</font></span>");
								    },
								    error: function(err) {
								    	$("#kreirajSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
										console.log(JSON.parse(err.responseText).userMessage);
								    }
								});
							}
		                }
		            },
		            error: function(err) {
		            	$("#kreirajSporocilo").html("<span class='obvestilo label " +
                    "label-danger fade-in'><font color='white'>Napaka '" +
                    JSON.parse(err.responseText).userMessage + "'!</font>");
		            }
		        });
		    }
		});
	}
}


/**
 * Za podan EHR ID preberi demografske podrobnosti pacienta in izpiši sporočilo
 * s pridobljenimi podatki (ime, priimek in datum rojstva).
 */
function preberiEHRodBolnika() {
	sessionId = getSessionId();

	var ehrId = $("#preberiEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiSporocilo").html("<span class='obvestilo label label-warning " +
      "fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
	    
	    var teza;
	    var visina;
	    var datumRojstva = $("#kreirajDatumRojstva").val();
	    var umes = datumRojstva.split("-");
        var starost = 2018 -umes[0];
        var Stlak;
        var Dtlak;
	    
		$.ajax({
            url: baseUrl + "/view/" + ehrId + "/height",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                visina = data[0].height;
                //console.log(data);
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/weight",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                teza = data[0].weight;
                //console.log(data);
            }
        });
        
        $.ajax({
            url: baseUrl + "/view/" + ehrId + "/blood_pressure",
            type: 'GET',
            headers: {
                "Ehr-Session": sessionId
            },
            success: function (data) {
                Stlak = data[0].systolic;
                Dtlak = data[0].diastolic;
                //console.log(data);
            }
        });
        
        $.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
	    		//console.log(data);
				var party = data.party;
				//console.log(party);
				//console.log(party.partyAdditionalInfo);
				var stRiskov = 0;
				
				var patientBMI = Math.round(teza / ((visina/100) * (visina/100)));
				//$("#preberiSporocilo").html("<span class='obvestilo label label-success fade-in'>Pacient '" + party.firstNames + " " + party.lastNames + "', s tezo " + teza + " , visino " + visina + " in BMI " + patientBMI + ".</span>");
			    if (patientBMI >= 25) {
			        stRiskov++;
			    }
			    if (starost >= 45) {
			        stRiskov++;
			    }
			    if (Stlak >= 130 || Dtlak >= 80) {
			        stRiskov++;
			    }
			    
			    if (stRiskov == 0) {
			        $("#rezultat").html("<img src='img/nizko.png' alt='Vas rezultat'>");
			    }
			    else if (stRiskov == 3) {
			        $("#rezultat").html("<img src='img/visoko.png' alt='Vas rezultat'>");
			    }
			    else {
			        $("#rezultat").html("<img src='img/srednje.png' alt='Vas rezultat'>");
			    }
			    
			},
			error: function(err) {
				$("#preberiSporocilo").html("<span class='obvestilo label label-danger fade-in'>Napaka '" + JSON.parse(err.responseText).userMessage + "'!");
				console.log(JSON.parse(err.responseText).userMessage);
			}
		});
	}
}








$(document).ready(function() {

  /**
   * Napolni testne vrednosti (ime, priimek in datum rojstva) pri kreiranju
   * EHR zapisa za novega bolnika, ko uporabnik izbere vrednost iz
   * padajočega menuja (npr. Pujsa Pepa).
   */
  $('#preberiPredlogoBolnika').change(function() {
    $("#kreirajSporocilo").html("");
    var podatki = $(this).val().split(",");
    $("#kreirajIme").val(podatki[0]);
    $("#kreirajPriimek").val(podatki[1]);
    $("#kreirajDatumRojstva").val(podatki[2]);
    $("#kreirajVision").val(podatki[3]);
    $("#kreirajTezo").val(podatki[4]);
    $("#kreirajStlak").val(podatki[5]);
    $("#kreirajDlak").val(podatki[6]);
  });

  /**
   * Napolni testni EHR ID pri prebiranju EHR zapisa obstoječega bolnika,
   * ko uporabnik izbere vrednost iz padajočega menuja
   * (npr. Dejan Lavbič, Pujsa Pepa, Ata Smrk)
   */
	$('#preberiObstojeciEHR').change(function() {
		$("#preberiSporocilo").html("");
		$("#preberiEHRid").val($(this).val());
	});


});